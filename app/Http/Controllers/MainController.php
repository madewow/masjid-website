<?php

namespace App\Http\Controllers;

class MainController extends Controller
{
    public function home()
    {
        $current_page = 'home';
        
        return view('front-end.pages.home', compact('current_page'));
    }
    
    public function explore()
    {
        $current_page = 'explore';
        
        return view('front-end.pages.explore', compact('current_page'));
    }
    
    public function detail()
    {
        $current_page = 'detail';
        
        return view('front-end.pages.detail', compact('current_page'));
    }
    
    public function detail_photography()
    {
        $current_page = 'photography';
        
        return view('front-end.pages.photography', compact('current_page'));
    }
}