@extends('front-end.templates._base')

@push('page-meta-tags')
<title></title>
@endpush

@push('body-class')
<body id="">
@endpush

@section('content')
<section class="grid-container full hero-image">
	<div class="background-image" data-interchange="[{{ asset('assets/img/hero-image-detail.jpg') }}, large]">
		<p>Masjid Al-Azhar</p>
		
		<div class="bar">
			<div class="grid-container full">
				<div class="grid-x grid-padding-x">
					<div class="cell large-6">
						<p>Masjid Al-Azhar<br>Jakarta<br>Masjid Raya</p>
					</div>
					
					<div class="cell large-6 grid-x align-bottom align-right">
						<ul class="share-navigation">
							<li>
								<a><i class="fab fa-facebook-f"></i></a>
							</li>
							
							<li>
								<a><i class="fab fa-twitter"></i></a>
							</li>
							
							<li>
								<a><i class="fab fa-instagram"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell large-6">
				<p class="lead">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
			</div>
			
			<div class="cell large-6">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco poriti laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in uienply voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat norin proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>
	</div>

	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell large-6">
				<ul class="icons-detail">
					<li class="year">1960</li>
					
					<li class="location">Jakarta</li>
					
					<li class="typology">Masjid Raya</li>
				</ul>
			</div>
			
			<div class="cell large-6">
				<ul class="detail-navigation">
					<li class="is-active">
						<a href="{{ url('/detail/photography') }}">Photography</a>
					</li>
					
					<li>
						<a href="{{ url('/detail/drawings') }}">Drawings</a>
					</li>
					
					<li>
						<a href="{{ url('/detail/history') }}">History</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
		</div>
	</div>
</section>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
@endpush