@extends('front-end.templates._base')

@push('page-meta-tags')
<title></title>
@endpush

@push('body-class')
<body id="">
@endpush

@section('content')
<section class="grid-container full hero-image">
	<div class="background-image" data-interchange="[{{ asset('assets/img/hero-image-home.jpg') }}, large]">
		<p>An Architectural Journey</p>
		
		<div class="bar">
			<div class="grid-container full">
				<div class="grid-x grid-padding-x">
					<div class="cell large-6">
						<p>Masjid Lorem Ipsum<br>Jakarta<br>Masjid Raya</p>
					</div>
					
					<div class="cell large-6 grid-x align-bottom align-right">
						<ul class="share-navigation">
							<li>
								<a><i class="fab fa-facebook-f"></i></a>
							</li>
							
							<li>
								<a><i class="fab fa-twitter"></i></a>
							</li>
							
							<li>
								<a><i class="fab fa-instagram"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<h2>INDONESIAN INSTITUTE FOR ISLAMIC CIVILIZATION</h2>
			</div>
		</div>
		
		<div class="grid-x grid-padding-x">
			<div class="cell large-4">
				<p>Lorem ipsum dolor sit amet, consectetur adipisifwcing elit, sed do eiusmod tempor incididunt ut labore et dolore roipi magna aliqua. Ut enim ad minim veeniam, quis nostruklad exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in tufpoy voluptate velit esse cillum dolore eu fugiat nulla parieratur. Excepteur sint occaecat cupidatat.</p>
				
				<img src="{{ asset('assets/img/content-home-01.jpg') }}">
			</div>
			
			<div class="cell large-4">
				<p>Culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptartem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi ropeior architecto beatae vitae dicta sunt explicabo. Nemo eniem ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eosep quiklop ratione voluptatem sequi nesciunt. Neque porro quisquam est, quepi dolorem ipsum quia dolor srit amet, consectetur adipisci velit, seid quia non numquam eiuris modi tempora incidunt ut labore et dolore magnam aliquam quaerat iope voluptatem.</p>
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisifwcing elit, sed do eiusmod tempor incididunt ut labore et dolore roipi magna aliqua. Ut enim ad minim veeniam, quis nostruklad exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
				
				<a class="cta">About us</a>
			</div>
			
			<div class="cell large-4">
				<figure>
					<img src="{{ asset('assets/img/content-home-02.jpg') }}">
					
					<figcaption>Lorem ipsum dolor sit amet</figcaption>
				</figure>
			</div>
		</div>
	</div>
</section>

<section id="about-us">
	<div class="content">
		<div class="grid-x align-middle">
		<div class="grid-container full">
			<div class="grid-x grid-padding-x">
				<div class="cell large-6">
					<h2>About Us</h2>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipisifwcing elit, sed do eiusmod tempor incididunt ut labore et dolore roipi magna aliqua. Ut enim ad minim veeniam, quis nostruklad exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in tufpoy voluptate velit esse cillum dolore eu fugiat nulla parieratur. Excepteur sint occaecat cupidatat.</p>
				</div>
				
				<form class="cell large-6">
					<input type="text" placeholder="Name">
					
					<input type="email" placeholder="Email">
					
					<input type="text" placeholder="Subject">
					
					<textarea placeholder="Message"></textarea>
					
					<button class="cta">Submit</button>
				</form>
			</div>
		</div>
		</div>
	</div>
</section>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
<script>
	$(document).ready(function(){
		$('.hero-image')
	});
</script>
@endpush