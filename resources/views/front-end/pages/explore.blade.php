@extends('front-end.templates._base')

@push('page-meta-tags')
<title></title>
@endpush

@push('body-class')
<body id="">
@endpush

@section('content')
<div class="grid-container full">
	<div class="grid-x grid-padding-x">
		<div class="cell auto">
			<ul id="timeline">
				<li>
					<a>1400</a>
				</li>
				
				<li>
					<a>1500</a>
				</li>
				
				<li>
					<a>1600</a>
				</li>
				
				<li>
					<a>1700</a>
				</li>
				
				<li>
					<a>1800</a>
				</li>
				
				<li>
					<a>1900</a>
				</li>
				
				<li>
					<a>2000</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="masonry">
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
		
		<p class="year">1429</p>
		
		<div class="box">
			<p><strong>Lorem Ipsum</strong><br>Jakarta<br>Nasional</p>
		</div>
	</div>
	
	<div class="item width-2">
		<img src="{{ asset('assets/img/grid-item-2x1.jpg') }}">
		
		<p class="year">1429</p>
		
		<div class="box">
			<p><strong>Lorem Ipsum</strong><br>Jakarta<br>Nasional</p>
		</div>
	</div>
	
	<div class="item width-2">
		<img src="{{ asset('assets/img/grid-item-2x2.jpg') }}">
		
		<p class="year">1429</p>
		
		<div class="box">
			<p><strong>Lorem Ipsum</strong><br>Jakarta<br>Nasional</p>
		</div>
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-2">
		<img src="{{ asset('assets/img/grid-item-2x1.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-2">
		<img src="{{ asset('assets/img/grid-item-2x2.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x2.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
	
	<div class="item width-2">
		<img src="{{ asset('assets/img/grid-item-2x1.jpg') }}">
	</div>
	
	<div class="item width-1">
		<img src="{{ asset('assets/img/grid-item-1x1.jpg') }}">
	</div>
</div>
@endsection

@push('page-styles')
@endpush

@push('page-scripts')
@endpush