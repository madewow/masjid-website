<header id="main-header">
	<div class="grid-container full">
		<div class="grid-x grid-padding-x">
			<div class="cell large-2 grid-x align-middle">
				<ul id="main-navigation" class="">
					<li>
						<a>About Us</a>
					</li>
					
					<li>
						<a>Contact Us</a>
					</li>	
				</ul>
				
				<a id="trigger-navigation">
					<span></span>
					<span></span>
				</a>
			</div>
			
			<div class="cell large-8 text-center">
				<img src="{{ asset('assets/img/logo-masjid.png') }}" class="logo">
			</div>

			<div class="cell large-2 grid-x align-middle align-right">
				<a class="btn-explore" data-toggle="offCanvas">Explore</a>
			</div>
		</div>
	</div>
</header>