<footer id="main-footer">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell large-3">
				<img src="{{ asset('assets/img/logo-masjid-footer.png') }}">
			</div>
			
			<div class="cell large-2">
				<ul class="footer-navigation">
					<li>
						<a>About Us</a>
					</li>
					
					<li>
						<a>Contact</a>
					</li>
					
					<li>
						<a>Terms &amp; Conditions</a>
					</li>
				</ul>
			</div>
			
			<div class="cell large-2">
				<ul class="social-media-navigation">
					<li>
						<a><i class="fab fa-facebook-f"></i> Facebook</a>
					</li>
					
					<li>
						<a><i class="fab fa-twitter"></i> Twitter</a>
					</li>
					
					<li>
						<a><i class="fab fa-instagram"></i> Instagram</a>
					</li>
				</ul>
			</div>
			
			<form class="cell large-3">
				<label>
					Subscribe to our newsletter
				</label>
				
				<div class="input-group">
					<input class="input-group-field" type="text">
					
					<div class="input-group-button">
						<button class="button">
							<i class="fas fa-search"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</footer>