<script src="{{ asset('assets/js/vendor/jquery.js') }}"></script>
<script src="{{ asset('assets/js/vendor/what-input.js') }}"></script>
<script src="{{ asset('assets/js/vendor/foundation.js') }}"></script>
<script src="{{ asset('assets/semantic/components/dropdown.js') }}"></script>
<script src="{{ asset('assets/semantic/components/transition.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.1/masonry.pkgd.min.js"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>