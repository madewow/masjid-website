<form id="main-aside">
	<fieldset>
		<legend>Period</legend>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1400">
			<label for="radio-period-1400">1400</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1500">
			<label for="radio-period-1500">1500</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1600">
			<label for="radio-period-1600">1600</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1700">
			<label for="radio-period-1700">1700</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1800">
			<label for="radio-period-1800">1800</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-1900">
			<label for="radio-period-1900">1900</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-period-2000">
			<label for="radio-period-2000">2000</label>
		</div>
	</fieldset>
	
	<label>
		Region
		
		<select id="region-dropdown" class="dropdown" data-prompt="Select region">
			<option value="">All</option>
			
			<option value="dki-jakarta">DKI Jakarta</option>
			
			<option value="banten">Banten</option>
		</select>
	</label>
	
	<fieldset>
		<legend>Typology</legend>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-typology-nasional">
			<label for="radio-typology-nasional">Nasional</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-typology-agung">
			<label for="radio-typology-agung">Agung</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-typology-raya">
			<label for="radio-typology-raya">Raya</label>
		</div>
		
		<div class="faux">
			<input type="radio" name="period" id="radio-typology-jami">
			<label for="radio-typology-jami">Jami</label>
		</div>
	</fieldset>
</form>