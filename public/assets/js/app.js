$(document).foundation();

$('#region-dropdown').dropdown();

$(document).on('click', '#trigger-navigation', function(){
	if($('#main-navigation').hasClass('is-active')) {
		$('#main-navigation').removeClass('is-active');
		
	} else {
		$('#main-navigation').addClass('is-active');
	}
});

var _column_width;

if (Foundation.MediaQuery.atLeast('large')) {
	_column_width = $('#masonry').width() / 6;
	
	console.log(_column_width);
}

$('#masonry').masonry({
  // options
  itemSelector: '.item',
  columnWidth: _column_width
});