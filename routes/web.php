<?php

Route::get('/explore', 'MainController@explore');
Route::get('/detail', 'MainController@detail');
Route::get('/detail/photography', 'MainController@detail_photography');
Route::get('/', 'MainController@home');
